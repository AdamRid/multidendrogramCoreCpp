#ifndef SMARTAXIS_H_INCLUDED
#define SMARTAXIS_H_INCLUDED

#include "../definitions/Dendrogram.h"

class SmartAxis
{
public:
	SmartAxis (Dendrogram& root, bool isUniformOrigin);
	double smartMin ();
	double smartMax ();
	double smartTicksSize ();

private:
	enum NiceType {NICE_FLOOR, NICE_CEIL, NICE_ROUND};
	double min_;
	double max_;
	double ticksSize;

	double getDendroMinHeight (Dendrogram& root, bool isUniformOrigin);
	double getDendroMaxHeight (Dendrogram& root, bool isUniformOrigin);
	void roundAxisLimits ();
	void roundTicks();
	double niceNum (double x, int nrange, NiceType round);
	int sign (double x);
};

#endif // SMARTAXIS_H_INCLUDED
