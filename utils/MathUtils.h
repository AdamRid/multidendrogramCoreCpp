#ifndef MATHUTILS_H
#define MATHUTILS_H

namespace MathUtils
{
    bool areEqual(int precision, double a, double b);
	double round (double number, int precision);
};



#endif
