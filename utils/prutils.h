#ifndef PRUTILS_H_INCLUDED
#define PRUTILS_H_INCLUDED

#include "../gbincludes.h"


template<class T>
void print(T data)
{
    cout << data;
}

template<class T>
void printl(T data)
{
    cout << data << endl;
}

enum ANSIColor
{
	BLACK, RED, GREEN, YELLOW, BLUE,
	MAGENTA, CYAN, WHITE
};

//
void printC(string data, ANSIColor color);

// trim from start (in place)
void ltrim(string &s);

// trim from end (in place)
void rtrim(string &s);

// trim from both ends (in place)
void trim(string &s);

bool startswith (string s, string pattern);



#endif // PRUTILS_H_INCLUDED
