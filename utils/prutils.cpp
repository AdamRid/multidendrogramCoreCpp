#include <iostream>
#include <vector>
#include <string>

#include "prutils.h"

using namespace std;

void printC(string data, ANSIColor color) {
    #ifndef _WIN32
    cout << "\033[1;" + std::to_string(30 + color) +
        "m" + data  + "\033[0m"<< endl;
    #else
    cout << data << endl;
    #endif // _WIN32
}

// trim from start (in place)
void ltrim(std::string &s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
void rtrim(std::string &s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
void trim(std::string &s)
{
    ltrim(s);
    rtrim(s);
}


bool startswith (std::string &s, std::string pattern)
{
    if (s.rfind(pattern, 0) == 0) return true;
    else return false;
}

bool startswith (std::string s, std::string pattern)
{
    if (s.rfind(pattern, 0) == 0) return true;
    else return false;
}

vector<string> split(const char *str, char c = ' ')
{
    vector<string> result;

    do
    {
        const char *begin = str;

        while(*str != c && *str)
            str++;

        result.push_back(string(begin, str));
    } while (0 != *str++);

    return result;
}


