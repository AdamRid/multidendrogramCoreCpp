#include "SmartAxis.h"
#include <limits>
#include <cmath>

const double NaN = std::numeric_limits<double>::quiet_NaN();

SmartAxis::SmartAxis (Dendrogram& root, bool isUniformOrigin)
{
    min_ = getDendroMinHeight (root, isUniformOrigin);
    max_ = getDendroMaxHeight (root, isUniformOrigin);
    roundAxisLimits ();
    roundTicks ();
}

double SmartAxis::smartMin ()
{
    return min_;
}

double SmartAxis::smartMax ()
{
    return max_;
}

double SmartAxis::smartTicksSize ()
{
    return ticksSize;
}

double SmartAxis::getDendroMinHeight (Dendrogram& root, bool isUniformOrigin)
{
    double bandsMinHeight = root.getBandsMinHeight ();
    double nodesMinHeight = root.getNodesMinHeight ();
    double minHeight;
    if (std::isnan (nodesMinHeight) || isUniformOrigin)
    {
        minHeight = bandsMinHeight;
    }
    else
    {
        minHeight = std::min(bandsMinHeight, nodesMinHeight);
    }
    if (root.isDistanceBased)
    {
        minHeight = std::min (minHeight, 0.0);
    }
    return minHeight;
}

double SmartAxis::getDendroMaxHeight (Dendrogram& root, bool isUniformOrigin)
{
    double bandsMaxHeight = root.getBandsMaxHeight ();
    double nodesMaxHeight = root.getNodesMaxHeight ();
    double maxHeight;
    if (std::isnan (nodesMaxHeight) || isUniformOrigin)
    {
        maxHeight = bandsMaxHeight;
    }
    else
    {
        maxHeight = std::max (bandsMaxHeight, nodesMaxHeight);
    }
    return maxHeight;
}

void SmartAxis::roundAxisLimits ()
{
    if (min_ == max_)
    {
        switch (sign(min_))
        {
            case 0:
                min_ = -1.0;
                max_ = +1.0;
                break;
            case +1:
                min_ /= 2.0;
                max_ *= 2.0;
                break;
            case -1:
                min_ *= 2.0;
                max_ /= 2.0;
                break;
        }
    }
    int nrange;
    if (sign(min_) == sign(max_))
    {
        nrange = (int) -std::rint (std::log10 (std::abs (
            2 * (max_ - min_) / (max_ + min_))));
        nrange = std::max (nrange, 0);
    }
    else
    {
        nrange = 0;
    }
    min_ = niceNum (min_, nrange, NiceType::NICE_FLOOR);
    max_ = niceNum (max_, nrange, NiceType::NICE_CEIL);
    if (sign (min_) == sign (max_))
    {
        if (max_ / min_ > 5.0)
        {
            min_ = 0.0;
        }
        else if (min_ / max_ > 5.0)
        {
            max_ = 0.0;
        }
    }
}

void SmartAxis::roundTicks()
{
    static int numTicks = 10;
    ticksSize = niceNum ((max_ - min_) / (numTicks - 1),
            0, NiceType::NICE_ROUND);
}

double SmartAxis::niceNum (double x, int nrange, NiceType round)
{
    long xsign;
    double f, y, fexp, rx, sx;

    if (x == 0.0)
    {
        return 0.0;
    }

    xsign = sign (x);
    x = std::abs (x);

    fexp = std::floor (std::log10 (x)) - nrange;
    sx = x / std::pow (10.0, fexp) / 10.0;	// scaled x
    rx = std::floor(sx);                   	// rounded x
    f = 10.0 * (sx - rx);	// fraction between 0 and 10

    if ((round == NiceType::NICE_FLOOR && xsign == +1) ||
        (round == NiceType::NICE_CEIL  && xsign == -1))
    {
        y = (int) std::floor (f);
    }
    else if ((round == NiceType::NICE_FLOOR && xsign == -1) ||
               (round == NiceType::NICE_CEIL && xsign == +1))
    {
        y = (int) std::ceil(f);
    }
    else
    {                               // round == NiceType.NICE_ROUND
        if (f < 1.5)
            y = 1;
        else if (f < 3.0)
            y = 2;
        else if (f < 7.0)
            y = 5;
        else
            y = 10;
    }

    sx = rx + (double) y / 10.0;

    return xsign * sx * 10.0 * std::pow(10.0, fexp);
}

int SmartAxis::sign (double x)
{
    if (x > 0)
        return 1;
    else if (x < 0)
        return -1;
    else
        return 0;
}

