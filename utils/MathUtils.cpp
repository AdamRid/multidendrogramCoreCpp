#include <cmath>
#include <math.h>

namespace MathUtils
{
    bool areEqual(int precision, double a, double b)
	{
        double factor = pow(10.0, (double) precision);
        return (round(a * factor) == round(b * factor));    // Revisar esto ya que es probable que no funciona esta comparación
    }

	double round (double number, int precision)
	{
		double factor = std::pow(10, precision);
		double x = number * factor;
		double r = std::round(x);
		if (r < x)
		{
			double epsilon = 1.0e-4;
			double rs = std::round(x + epsilon);
			if (rs - r > 0.5)
			{
				r = rs;
			}
		}
		return r / factor;
	}
};

