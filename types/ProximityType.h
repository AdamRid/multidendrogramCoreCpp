#ifndef PROXIMITYTYPE_H
#define PROXIMITYTYPE_H

enum ProximityType
{
	DISTANCE, SIMILARITY
};

#endif
