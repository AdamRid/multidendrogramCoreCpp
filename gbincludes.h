#ifndef GBINCLUDES_H
#define GBINCLUDES_H

#include <iostream>
#include <fstream>
#include <cmath>
#include <iomanip>		// setprecision method
#include <string>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include <vector>
#include <map>
#include <limits>
#include <list>

#include <algorithm>
#include <cctype>
#include <locale>
#include <chrono>

using std::cout;
using std::endl;
using std::string;
using std::map;
using std::vector;
using std::map;
using std::list;
using std::numeric_limits;
using namespace std::chrono;

const double infinity = std::numeric_limits<double>::infinity ();
const double doubleNaN = std::numeric_limits<double>::quiet_NaN();
const double epsilon = std::numeric_limits<double>::epsilon();

#ifdef _WIN32
const string INPUT_PATH = "iodata\\input_data\\";
const string OUTPUT_PATH = "iodata\\output_data\\";
#elif
const string INPUT_PATH = "iodata/input_data/";
const string OUTPUT_PATH = "iodata/output_data/";
#endif

#include "./utils/prutils.h"

#endif
