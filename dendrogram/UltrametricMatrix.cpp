#include "../gbincludes.h"

#include "UltrametricMatrix.h"


UltrametricMatrix::UltrametricMatrix(Dendrogram& root, vector<string>& externLabels,
        OriginType originType)
{
    this->precision = root.precision;
    this->labels = externLabels;
    this->hashLabels = this->getSorting(externLabels);
    bool isUniformOrigin =(originType == UNIFORM_ORIGIN) ? true : false;
    SmartAxis smartAxis(root, isUniformOrigin);
    double dendroBottomHeight = root.isDistanceBased? smartAxis.smartMin() : smartAxis.smartMax();
    SymmetricMatrix sm(root.numberOfLeaves());
    this->ultraMatrix = sm;
    calculateUltrametricMatrix(root, dendroBottomHeight, isUniformOrigin);
}

map<string, int>& UltrametricMatrix::getSorting(vector<string>& externLabels)
{
    map<string, int>& sorting = *new map<string, int>();
    for (int i = 0; i <(int)externLabels.size(); i++)
    {
        sorting[externLabels.at(i)] = i;;
    }
    return sorting;
}

void UltrametricMatrix::calculateUltrametricMatrix(Dendrogram& cluster,
        double dendroBottomHeight, bool isUniformOrigin)
{
    int numSubclusters = cluster.numberOfSubclusters();
    if (numSubclusters == 1)
    {
        double clusterBottomHeight = cluster.getRootBottomHeight();
        double clusterHeight =
               (std::isnan(clusterBottomHeight) || isUniformOrigin) ?
                MathUtils::round(dendroBottomHeight, this->precision) :
                MathUtils::round(clusterBottomHeight, this->precision);
        int i = this->hashLabels[cluster.getLabel()];
        this->ultraMatrix.setElement(i, i, clusterHeight);
    }
    else
    {
        double clusterInternalHeight = cluster.getRootInternalHeight();
        double clusterHeight = MathUtils::round(clusterInternalHeight,
                this->precision);
        int numLeaves = cluster.numberOfLeaves();
        for (int m = 0; m < numLeaves - 1; m ++)
        {
            Dendrogram& ci = cluster.getLeaf(m);
            int i = this->hashLabels[ci.getLabel()];
            for (int n = m + 1; n < numLeaves; n ++)
            {
                Dendrogram& cj = cluster.getLeaf(n);
                int j = this->hashLabels[cj.getLabel()];
                this->ultraMatrix.setElement(i, j, clusterHeight);
            }
        }
        for (int n = 0; n < numSubclusters; n ++)
        {
            calculateUltrametricMatrix(cluster.getSubcluster(n),
                    dendroBottomHeight, isUniformOrigin);
        }
    }
}

SymmetricMatrix& UltrametricMatrix::getMatrix()
{
    return(this->ultraMatrix);
}

vector<string>& UltrametricMatrix::getLabels()
{
    return this->labels;
}

void UltrametricMatrix::saveAsTxt(string path)
{
    std::ofstream file(path);
    if (file.is_open())
    {
        string str = "";
        for (int i = 0; i <(int) this->labels.size(); i++)
        {
            str += this->labels.at(i) + "\t";
        }
        file << str + "\n";
        for (int i = 0; i < this->ultraMatrix.numberOfRows(); i++)
        {
            str = "";
            for (int j = 0; j < this->ultraMatrix.numberOfRows(); j++)
            {
                double clusterHeight = this->ultraMatrix.getElement(i, j);
                char buffer[10];
                sprintf(buffer, "%1.6f", clusterHeight);
                str += buffer;
                str += "\t";
            }
            file << str + "\n";
        }

        file.close();
    }
}
