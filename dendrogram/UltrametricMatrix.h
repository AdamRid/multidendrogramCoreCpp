#ifndef ULTRAMETRICMATRIX_H
#define ULTRAMETRICMATRIX_H

#include "../gbincludes.h"

#include "../utils/SmartAxis.h"
#include "../utils/MathUtils.h"
#include "../definitions/SymmetricMatrix.h"
#include "../definitions/Dendrogram.h"
#include "../types/OriginType.h"

class UltrametricMatrix
{
public:
    UltrametricMatrix(Dendrogram& root, vector<string>& externLabels,
			OriginType originType);
    SymmetricMatrix& getMatrix();
    void saveAsTxt(string path);
    vector<string>& getLabels();

private:
    int precision;
    vector<string> labels;
    map<string, int> hashLabels;
    SymmetricMatrix ultraMatrix;

    map<string, int>& getSorting(vector<string>& externLabels);
    void calculateUltrametricMatrix(Dendrogram& cluster,
			double dendroBottomHeight, bool isUniformOrigin);

};

#endif
