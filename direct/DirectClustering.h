#ifndef DIRECTCLUSTERING_H
#define DIRECTCLUSTERING_H

#include "../gbincludes.h"

#include "../types/ProximityType.h"
#include "../types/MethodType.h"
#include "../types/OriginType.h"
#include "../types/MethodType.h"
#include "../dendrogram/UltrametricMatrix.h"
#include "../data/ExternalData.h"
#include "../data/DataFile.h"
#include "../clusterings/HierarchicalClustering.h"
#include "../clusterings/VersatileLinkage.h"
#include "../clusterings/SingleLinkage.h"
#include "../clusterings/CompleteLinkage.h"
#include "../clusterings/Centroid.h"
#include "../clusterings/Ward.h"
#include "../clusterings/BetaFlexible.h"
#include "../definitions/SymmetricMatrix.h"

class DirectClustering
{
public:
    DirectClustering (string filename, ProximityType proximityType,
    int initialPrecision, MethodType methodType, double methodParameter,
    bool isWeighted, OriginType originType);
    HierarchicalClustering* newClustering(MethodType methodType,
			SymmetricMatrix& proximityMatrix, vector<string>& labels, ProximityType proximityType,
			int precision, bool isWeighted, double methodParameter);

    UltrametricMatrix& getUltrametricMatrix ();
    ~DirectClustering ();

private:
	DataFile* dataFile;
	ExternalData* externalData;
	ProximityType proximityType;
	int precision;
	string filePrefix;
	OriginType originType;
	HierarchicalClustering* clustering;
	UltrametricMatrix* ultraMatrix = NULL;
	// DendrogramMeasures* dendroMeasures;

	double inverseSigmoid(double y);

public:
    const int AUTO_PRECISION = numeric_limits<int>::min();;
	const string MEASURES_SUFIX = "-measures.txt";
	const string ULTRAMETRIC_SUFIX = "-ultrametric.txt";
	const string GRAPH_SUFIX = "-graph.net";
	const string TXT_TREE_SUFIX = "-tree.txt";
	const string NEWICK_TREE_SUFIX = "-newick.txt";
	const string JSON_TREE_SUFIX = ".json";
};


#endif
