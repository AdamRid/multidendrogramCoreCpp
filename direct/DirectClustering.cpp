#include "DirectClustering.h"
#include "../utils/prutils.h"

DirectClustering::DirectClustering (string filename, ProximityType proximityType,
    int initialPrecision, MethodType methodType, double methodParameter,
    bool isWeighted, OriginType originType)
{
    this->dataFile = new DataFile (filename);
    this->externalData = new ExternalData (*this->dataFile);
    this->proximityType = proximityType;
    SymmetricMatrix& proximityMatrix = this->externalData->getProximityMatrix();
    this->precision = 7; // Forzamos la precision a 7 dado que el getPrecision aun no esta implementado
    this->originType = originType;

    if ((proximityMatrix.minimumValue() < 0.0) &&
        (methodType == VERSATILE_LINKAGE || methodType == GEOMETRIC_LINKAGE))
    {
        perror("Clustering algorithm incompatible with negative data");
        abort();
    }
    this->clustering = newClustering(methodType, proximityMatrix, this->externalData->getNames(),
				proximityType, this->precision, isWeighted, methodParameter);
    this->clustering->build();
}

UltrametricMatrix& DirectClustering::getUltrametricMatrix ()
{
    if (this->ultraMatrix == NULL)
    {
        this->ultraMatrix = new UltrametricMatrix(this->clustering->getRoot(),
            this->externalData->getNames(), this->originType);
    }
    return *(this->ultraMatrix);
}


HierarchicalClustering* DirectClustering::newClustering(MethodType methodType,
        SymmetricMatrix& proximityMatrix, vector<string>& labels, ProximityType proximityType,
        int precision, bool isWeighted, double methodParameter)
{
    bool isDistanceBased = proximityType == DISTANCE ? true : false;
    double power;
    HierarchicalClustering* clustering;
    switch (methodType)
    {
        case VERSATILE_LINKAGE:
            power = inverseSigmoid(methodParameter);
            clustering = new VersatileLinkage(proximityMatrix, labels, isDistanceBased, precision, isWeighted, power);
            break;
        case SINGLE_LINKAGE:
            clustering = new SingleLinkage(proximityMatrix, labels, isDistanceBased, precision);
            break;
        case COMPLETE_LINKAGE:
            clustering = new CompleteLinkage(proximityMatrix, labels, isDistanceBased, precision);
            break;
        case ARITHMETIC_LINKAGE:
            power = +1.0;
            clustering = new VersatileLinkage(proximityMatrix, labels, isDistanceBased, precision, isWeighted, power);
            break;
        case GEOMETRIC_LINKAGE:
            power = 0.0;
            clustering = new VersatileLinkage(proximityMatrix, labels, isDistanceBased, precision, isWeighted, power);
            break;
        case HARMONIC_LINKAGE:
            power = -1.0;
            clustering = new VersatileLinkage(proximityMatrix, labels, isDistanceBased, precision, isWeighted, power);
            break;
        case CENTROID:
            clustering = new Centroid(proximityMatrix, labels, isDistanceBased, precision, isWeighted);
            break;
        case WARD:
            clustering = new Ward(proximityMatrix, labels, isDistanceBased, precision);
            break;
        case BETA_FLEXIBLE:
            clustering = new BetaFlexible(proximityMatrix, labels, isDistanceBased, precision, isWeighted, methodParameter);
            break;
        default:
            clustering = NULL;
            break;
    }
    return clustering;
}


double DirectClustering::inverseSigmoid(double y)
{
    if (y <= -1.0)
    {
        return -std::numeric_limits<double>::infinity();
    }
    else if (y >= +1.0)
    {
        return  std::numeric_limits<double>::infinity();
    }
    else
    {
        double y1 = 0.001;	// 0 < y1 = sigmoid(1) < 1
        return log((1.0 + y) / (1.0 - y))
             / log((1.0 + y1) / (1.0 - y1));
    }
}

DirectClustering::~DirectClustering ()
{
    delete this->dataFile;
    delete this->externalData;
    // delete this->clustering;
    if (this->ultraMatrix != NULL)
    {
        delete this->ultraMatrix;
    }
}

