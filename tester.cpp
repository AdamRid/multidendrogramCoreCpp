/*
    Multidendrogram Core C++
    version = 0.1.0
    Authors = Sergio Gomez, Alberto Fernande, Adam Ridouane
*/

#include "gbincludes.h"
#include "definitions/Dendrogram.h"
#include "definitions/SymmetricMatrix.h"
#include "clusterings/VersatileLinkage.h"
#include "utils/SmartAxis.h"
#include "utils/prutils.h"
#include "data/DataFile.h"
#include "direct/DirectClustering.h"
#include "types/ProximityType.h"
#include "types/MethodType.h"
#include "clusterings/VariableGroupSAHN.h"

map<MethodType, string> METHODS = {
    {VERSATILE_LINKAGE, "VERSATILE_LINKAGE"},
    {SINGLE_LINKAGE, "SINGLE_LINKAGE"},
    {COMPLETE_LINKAGE, "COMPLETE_LINKAGE"},
    {ARITHMETIC_LINKAGE, "ARITHMETIC_LINKAGE"},
    {GEOMETRIC_LINKAGE, "GEOMETRIC_LINKAGE"},
    {HARMONIC_LINKAGE, "HARMONIC_LINKAGE"},
    {CENTROID, "CENTROID"},
    {WARD, "WARD"},
    {BETA_FLEXIBLE, "BETA_FLEXIBLE"}
};

void runClustering (string fileName, ProximityType proximityType,
    int precision, MethodType methodType, double p, bool isWeighted,
    OriginType originType)
{
    DirectClustering dirClus(fileName, proximityType, precision,
        methodType, p, isWeighted, originType);
    UltrametricMatrix um = dirClus.getUltrametricMatrix();
    DataFile df(fileName);
    string outputFile = OUTPUT_PATH + df.getNameNoExt() + "_" + METHODS[methodType] + ".txt";
    um.saveAsTxt(outputFile);
}

void multidendroTester()
{
    ProximityType proximityType = DISTANCE;
    int precision = std::numeric_limits<int>::min();
    bool isWeighted = false;
    OriginType originType = UNIFORM_ORIGIN;
    double p = 0.0;
    vector<string> filenames({
        INPUT_PATH + "NYSE 2001-2003 distances.txt",
        INPUT_PATH + "NYSE 2015-2017 distances 0010.txt",
        INPUT_PATH + "NYSE 2015-2017 distances 0100.txt",
    });
    string fileName = filenames.at(2);
    vector<MethodType> methods = {
        VERSATILE_LINKAGE, SINGLE_LINKAGE, COMPLETE_LINKAGE,
        ARITHMETIC_LINKAGE, GEOMETRIC_LINKAGE, HARMONIC_LINKAGE,
        CENTROID, WARD, BETA_FLEXIBLE
    };
    for (MethodType methodType: methods)
    {
        printC("\nEjecutando " + METHODS[methodType], BLUE);
        int numTests = 1; // Incrementar valor para refinar el tiempo medio
        auto start = std::chrono::steady_clock::now();
        double end_ = 0.0;
        for (int n = 1; n <= numTests; n++)
        {
            start = std::chrono::steady_clock::now();
            cout << "Iteracion actual: " << n << endl;
            runClustering (fileName, proximityType, precision,
                methodType, p, isWeighted, originType);
            end_ +=(std::chrono::duration_cast<std::chrono::milliseconds>
                    (std::chrono::steady_clock::now() - start).count()) / numTests;
        }
        cout << "Tiempo medio en milisegundos: " << end_ << "ms" << endl;
        cout << "Tiempo medio en segundos: " << end_/1000.0 << " s" << endl;
    }
}

int main()
{
    printC("Iniciando ejecucion", GREEN);
    multidendroTester();
    printC("Fin de la ejecucion", GREEN);
    return 0;
}
