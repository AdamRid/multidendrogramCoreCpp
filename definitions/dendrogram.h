#ifndef DENDROGRAM_H
#define DENDROGRAM_H
#include "../gbincludes.h"

#include <string>
#include <vector>

using std::string;

class Dendrogram
{
public:
	Dendrogram (int identifier,const string& label, bool isDistanceBased,
		int precision);
    Dendrogram(const Dendrogram& d);
    Dendrogram();
	int getIdentifier ();
	string getLabel ();
	void setRootHeights (const double height);
	double getRootBottomHeight ();
	void setRootTopHeight (const double rootTopHeight);
	double getRootTopHeight ();
	void setNodesHeights (const double height);
	void setBandsHeights (const double height);
	double getNodesMinHeight ();
	double getNodesMaxHeight ();
	double getBandsMinHeight ();
	double getBandsMaxHeight ();
	void setSupercluster (bool isSupercluster);
	bool isSupercluster ();
	void addSubcluster (Dendrogram& subc);
	int numberOfSubclusters ();
	int numberOfSubroots ();
	int numberOfLeaves ();
	Dendrogram& getSubcluster (int position);
	Dendrogram& getSubroot (int position);
	Dendrogram& getLeaf (int position);
	double normalizedTreeBalance ();
	double getRootInternalHeight();
	void setRootInternalHeight(const double rootInternalHeight);

private:

	// Variables that define the dendrogram root
	int identifier;
	string label;
	double rootBottomHeight;
	double rootInternalHeight;
	double rootTopHeight;

	// Global minimum and maximum heights
	double nodesMinHeight;
	double nodesMaxHeight;
	double bandsMinHeight;
	double bandsMaxHeight;
	// To know if it is a supercluster
	bool misSupercluster;
	// Lists
	vector<Dendrogram> subclustersList;
	vector<Dendrogram> leavesList;

	vector<Dendrogram>& getLeavesList ();
	double minimumTreeBalance ();
	class EntropyTuple
	{
    public:
        double sumEntropy;
        int numJunctions;
        EntropyTuple ();
    };
	EntropyTuple* accumulateEntropy ();
	double rootEntropy ();

public:
	// Parameters used to build the dendrogram
	bool isDistanceBased;
	int precision;


};

#endif
