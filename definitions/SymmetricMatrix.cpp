
#include "../gbincludes.h"
#include "SymmetricMatrix.h"


const double NaN = std::numeric_limits<double>::quiet_NaN();

SymmetricMatrix::SymmetricMatrix() {}

SymmetricMatrix::SymmetricMatrix (SymmetricMatrix& original)
{
  this->diagonal = original.getDiagonal();
  this->lowerTriangle = original.getLowerTriangle();
  this->minValue = original.minimumValue();
  this->maxValue = original.maximumValue();
  this->maxDecimals = original.getPrecision();
}

SymmetricMatrix::SymmetricMatrix (vector<double>& lowerTriangle)
{
    maxDecimals = 0;
    int numElements = lowerTriangle.size ();

    int numRows = (1 + (int) sqrt (1 + 8 * numElements)) / 2;
    diagonal.reserve(numRows);
    for (int i = 0; i < (int) diagonal.capacity (); i++)
        diagonal.push_back (NaN);

    this->lowerTriangle.reserve (numElements);
    for (int i = 0; i < numElements; i++)
        this->lowerTriangle.push_back (lowerTriangle[i]);

    double value;
    for (int index = 0; index < numElements; index++)
    {
        value = lowerTriangle[index];
        minValue = std::min (minValue, value);
        maxValue = std::max (maxValue, value);
        countDecimals (value);
    }
}

SymmetricMatrix::SymmetricMatrix (int numRows)
{
    diagonal.reserve(numRows);
    for (int i = 0; i < (int) diagonal.capacity (); i++)
        diagonal.push_back (NaN);

    int numElements = (numRows - 1) * numRows / 2;
    lowerTriangle.reserve (numElements);
    for (int i = 0; i < (int) lowerTriangle.capacity (); i++)
        lowerTriangle.push_back (NaN);
};

vector<double>& SymmetricMatrix::getDiagonal()
{
    return this->diagonal;
}

vector<double>& SymmetricMatrix::getLowerTriangle()
{
    return this->lowerTriangle;
}

void SymmetricMatrix::setElement (int i, int j, double value)
{
    if (i == j)
    {
        diagonal[i] = value;
    }
    else
    {
        int index = (i > j) ? getIndex (i, j) : getIndex (j, i);
        lowerTriangle[index] = value;
        minValue = std::min(minValue, value);
        maxValue = std::max(maxValue, value);
    }
}

double SymmetricMatrix::getElement (int i, int j)
{
    if (i == j)
    return diagonal[i];
    else
    {
      int index = (i > j) ? getIndex (i, j) : getIndex (j, i);
      return lowerTriangle[index];
    }
}

int SymmetricMatrix::numberOfRows ()
{
    return diagonal.size ();
}

double SymmetricMatrix::minimumValue ()
{
    return minValue;
}

double SymmetricMatrix::maximumValue ()
{
    return maxValue;
}

int SymmetricMatrix::getPrecision ()
{
    return maxDecimals;
}

void SymmetricMatrix::countDecimals (double value)
{
    // PENDIENTE DE IMPLEMENTACIÓN
}

int SymmetricMatrix::getIndex (int i, int j)
{
    int n =  diagonal.size ();
    return (2 * n - j - 1) * j / 2 + i - j - 1;
}


void SymmetricMatrix::square()
{
    for (int i = 0; i < (int)this->diagonal.size(); i ++) {
        this->diagonal[i] = this->diagonal[i] * this->diagonal[i];
    }
    this->minValue = infinity;
    this->maxValue = -infinity;
    for (int index = 0; index < (int)this->lowerTriangle.size(); index ++) {
        this->lowerTriangle[index] =
                this->lowerTriangle[index] * this->lowerTriangle[index];
        this->minValue = std::min(this->minValue, this->lowerTriangle[index]);
        this->maxValue = std::max(this->maxValue, this->lowerTriangle[index]);
    }
}
