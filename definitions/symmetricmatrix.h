#ifndef SYMMETRICMATRIX_H
#define SYMMETRICMATRIX_H

#include "../gbincludes.h"

class SymmetricMatrix
{
public:
  SymmetricMatrix (SymmetricMatrix& original);
  SymmetricMatrix (vector<double>& lowerTriangle);
  SymmetricMatrix (double lowerTriangle);
  SymmetricMatrix (int numRows);
  SymmetricMatrix ();
  void setElement (int i, int j, double value);
  double getElement (int i, int j);
  int numberOfRows ();
  double minimumValue ();
  double maximumValue ();
  int getPrecision ();
  vector<double>& getDiagonal ();
  vector<double>& getLowerTriangle ();
  int getMaxDecimals ();
  void square();

private:
  vector<double> diagonal;			// Diagonal elements
  vector<double> lowerTriangle;	// Lower triangular elements
										// by columns
  double minValue = infinity;	// Minimum and maximum lower triangular values
  double maxValue = -infinity;	// Maximum number of decimal digits in any element
  int maxDecimals;

  void countDecimals (double value);
  int getIndex (int i, int j);
};

#endif
