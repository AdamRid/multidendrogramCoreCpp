#include "Dendrogram.h"

#include "../gbincludes.h"

const double NaN = std::numeric_limits<double>::quiet_NaN();


// ---REVISADO
Dendrogram::Dendrogram (int identifier,const string& label, bool isDistanceBased,
    int precision)
{
    rootBottomHeight = NaN;
    rootTopHeight = NaN;
    nodesMinHeight = NaN;
    nodesMaxHeight = NaN;
    bandsMinHeight = NaN;
    bandsMaxHeight = NaN;
    misSupercluster = true;

    this->identifier = identifier;
    this->label= label;
    this->isDistanceBased = isDistanceBased;
    this->precision = precision;
}

Dendrogram::Dendrogram (const Dendrogram& d)
{
    // Variables that define the dendrogram root
	this->identifier = d.identifier;
	this->label =  d.label;
	this->rootBottomHeight = d.rootBottomHeight;
	this->rootInternalHeight =  d.rootInternalHeight;
	this->rootTopHeight = d.rootTopHeight;

	// Global minimum and maximum heights
	this->nodesMinHeight = d.nodesMinHeight;
	this->nodesMaxHeight = d.nodesMaxHeight;
	this->bandsMinHeight = d.bandsMinHeight;
	this->bandsMaxHeight = d.bandsMaxHeight;
	// To know if it is a supercluster
	this->misSupercluster = d.misSupercluster;
	// Lists vector<Dendrogram>
	this->subclustersList =  d.subclustersList;
	this->leavesList = d.leavesList;

	this->isDistanceBased = d.isDistanceBased;
	this->precision = d.precision;
}

Dendrogram::Dendrogram ()
{
}

int Dendrogram::getIdentifier ()
{
    return identifier;
}

string Dendrogram::getLabel ()
{
    return label;
}

void Dendrogram::setRootHeights (const double height)
{
    rootBottomHeight = height;
    rootTopHeight = height;
    rootInternalHeight = height;
}

double Dendrogram::getRootBottomHeight ()
{
    return rootBottomHeight;
}

void Dendrogram::setRootTopHeight (const double rootTopHeight)
{
    this->rootTopHeight = rootTopHeight;
    this->bandsMinHeight = std::min (this->bandsMinHeight,
    rootTopHeight);
    this->bandsMaxHeight = std::max (this->bandsMaxHeight,
    rootTopHeight);
}

double Dendrogram::getRootTopHeight ()
{
    return rootTopHeight;
}

void Dendrogram::setNodesHeights (const double height)
{
    nodesMinHeight = height;
    nodesMaxHeight = height;
}

void Dendrogram::setBandsHeights (const double height)
{
    bandsMinHeight = height;
    bandsMaxHeight = height;
}

double Dendrogram::getNodesMinHeight ()
{
    return nodesMinHeight;
}

double Dendrogram::getNodesMaxHeight ()
{
    return nodesMaxHeight;
}

double Dendrogram::getBandsMinHeight ()
{
    return bandsMinHeight;
}

double Dendrogram::getBandsMaxHeight ()
{
    return bandsMaxHeight;
}

void Dendrogram::setSupercluster (bool isSupercluster)
{
    misSupercluster = isSupercluster;
}

bool Dendrogram::isSupercluster ()
{
    return misSupercluster;
}

void Dendrogram::addSubcluster (Dendrogram& subc)
{
    subclustersList.emplace_back (subc);
    if (subc.numberOfSubclusters() == 1)
    {
        leavesList.emplace_back (subc);
    }
    else
    {
        for (Dendrogram& elem: subc.getLeavesList ())
        {
            leavesList.emplace_back (elem);
        }
    }
    if (std::isnan (nodesMinHeight))
    {
        nodesMinHeight = subc.nodesMinHeight;
    }
    else if (!std::isnan (subc.nodesMinHeight))
    {
        nodesMinHeight =
            std::min (nodesMinHeight, subc.nodesMinHeight);
    }
    if (std::isnan (nodesMaxHeight))
    {
        nodesMaxHeight = subc.nodesMaxHeight;
    }
    else if (!std::isnan (subc.nodesMaxHeight))
    {
        nodesMaxHeight =
            std::max(nodesMaxHeight, subc.nodesMaxHeight);
    }
    if (!std::isnan (subc.bandsMinHeight))
    {
        bandsMinHeight =
            std::min(bandsMinHeight, subc.bandsMinHeight);
    }
    if (!std::isnan (subc.bandsMaxHeight))
    {
        bandsMaxHeight =
            std::max(bandsMaxHeight, subc.bandsMaxHeight);
    }
}

int Dendrogram::numberOfSubclusters ()
{
    if (subclustersList.size () == 0)
    {
        return 1;
    }
    else
    {
        return subclustersList.size ();
    }
}

int Dendrogram::numberOfSubroots ()
{
    if (misSupercluster)
    {
        return subclustersList.size ();
    }
    else
    {
        return 1;
    }
}

Dendrogram& Dendrogram::getSubcluster (int position)
{

    Dendrogram* c = NULL; 	// VERIFICAR TRATAMIENTO QUE SE REALIZA EN LA
                    // RUTINA QUE REALIZA LA LLAMADA AL METODO
                    // BUSCAR UNA ALTERNATIVA QUE NO INCIALIZE "c" A NULL
    if (subclustersList.empty () && position == 0)
    {
        c = this;
    }
    else if (position < (int) subclustersList.size ())
    {
        c = &subclustersList.at(position);
    }

    return *c;
}

Dendrogram& Dendrogram::getSubroot (int position)
{
    Dendrogram* c = NULL;
    if (misSupercluster)
    {
        c = &subclustersList.at(position);
    }
    else if (position == 0)
    {
        c = this;
    }

    return *c;
}

int Dendrogram::numberOfLeaves ()
{
    if (leavesList.size () == 0)
    {
        return 1;
    }
    else
    {
        return leavesList.size ();
    }
}

Dendrogram& Dendrogram::getLeaf (int position)
{
    Dendrogram* c = NULL;
    if (leavesList.empty () && position == 0)
    {
        c = this;

    }
    else if (position < (int) leavesList.size ())
    {
        c = &leavesList.at(position);
    }
    return *c;
}

double Dendrogram::normalizedTreeBalance ()
{
    EntropyTuple* et = this->accumulateEntropy ();
    if (et->numJunctions == 0)
    {
        return 0.0;
    }
    else
    {
        double balance = et->sumEntropy / (double) et->numJunctions;
        double minBalance = minimumTreeBalance ();
        return (balance - minBalance) / (1.0 - minBalance);
    }
    delete et;
}


vector<Dendrogram>& Dendrogram::getLeavesList ()
{
    return leavesList;
}

double Dendrogram::minimumTreeBalance ()
{
    int numLeaves = numberOfLeaves ();
    double balance = std::log (numLeaves);

    for (int n = 2; n < numLeaves; n++)
    {
        balance += std::log (n) / (double) (n + 1);
    }
    return balance / ((double) (numLeaves - 1) * std::log (2));
}

Dendrogram::EntropyTuple::EntropyTuple ()
{
    sumEntropy = 0.0;
    numJunctions = 0;
}

Dendrogram::EntropyTuple* Dendrogram::accumulateEntropy ()
{
    EntropyTuple* et = new EntropyTuple ();
    int numSubclusters = numberOfSubclusters ();
    if (numSubclusters > 1)
    {
        et->sumEntropy = rootEntropy ();
        et->numJunctions = 1;
        for (int i = 0; i < numSubclusters; i++)
        {
            Dendrogram& cI = subclustersList.at (i);
            EntropyTuple* etI = cI.accumulateEntropy ();
            et->sumEntropy += etI->sumEntropy;
            et->numJunctions += etI->numJunctions;
            delete etI;
        }
    }
    return et;
}

double Dendrogram::rootEntropy ()
{
    double h = 0.0;
    int numSubclusters = numberOfSubclusters ();
    if (numSubclusters > 1)
    {
        int numLeaves = numberOfLeaves ();
        for (int i = 0; i < numSubclusters; i++)
        {
            Dendrogram& cI = subclustersList.at (i);
            double p = (double) cI.numberOfLeaves () / (double)
                numLeaves;
            h -= p * std::log (p);
        }
        h /= std::log (numSubclusters);
    }

    return h;
}


double Dendrogram::getRootInternalHeight()
{
    return this->rootInternalHeight;
}

void Dendrogram::setRootInternalHeight(const double rootInternalHeight)
{
	this->rootInternalHeight = rootInternalHeight;
}
