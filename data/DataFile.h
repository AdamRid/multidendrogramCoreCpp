#ifndef DATAFILE_H
#define DATAFILE_H

#include "../gbincludes.h"

class DataFile
{
public:
    DataFile();
    DataFile(const string& fileName);
    DataFile(const string& name,const string& path);
    DataFile(DataFile& fd);
    DataFile& operator =(DataFile& fd);
    string getName();
    string getNameNoExt();
    void setName(const string& name);
    string getPath();
    void setPath(const string& path);
    string getPathName();
    string getPathNameNoExt();

private:
    string name;
    string path;
    string nameNoExt;

    string getFileNameWithoutExtension(const string& fileName);
    string extractPathName(const string& fileName);
    string extractFileName(const string& fileName);
};

#endif
