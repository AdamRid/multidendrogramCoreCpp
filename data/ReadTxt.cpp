#include "ReadTxt.h"

#include "../gbincludes.h"

ReadTxt::ReadTxt(const string& filePath)
{
    this->fileName = filePath;
    saveInMemory();
    int numLines = this->dataList.size();
    int numCols = dataList.at(0).size();
    if (numCols != 3)
    {
        this->data = &this->readMatrix();
    }
    else
    {
        if ((numLines != 3) &&(numLines != 4))
        {
            data = &readList();
        }
        else
        {
            bool typeL, typeM;
            vector<ProximityPair<string>> *lstL = NULL;
            vector<ProximityPair<string>> *lstM = NULL;
            try
            {
                typeL = true;
                *lstL = readList();
            }
            catch(int e)
            {
                typeL = false;
                lstL = NULL;
            }
            try
            {
                typeM = true;
                *lstM = readList();
            }
            catch(int e)
            {
                typeM = false;
                //lstM = NULL;
            }

            if (typeL && typeM)
            {
                perror("Ambiguous file. It will be taken as a list-like file");
                data = lstL;
            }
            else if (typeL)
            {
                data = lstL;
            }
            else if (typeM)
            {
                data = lstM;
            }
            else
            {
                perror("The loaded file is not compatible with this application");
                abort();
            }
        }
    }

}

int ReadTxt::getNumElements()
{
    return this->numElements;
}

vector<ProximityPair<string>>& ReadTxt::getData()
{
    return *this->data;
}

void ReadTxt::saveInMemory()
{
    std::ifstream file(this->fileName);
    if (!file.is_open())
    {
        std::cout << "unable to open the file: " << this->fileName << std::endl;
        abort();
    }
    int numLine = 0;
    string line;
    while(getline(file, line))
    {
        trim(line);
        if (!(line == "") && !startswith(line, "#"))
        {
            numLine++;
            this->dataList.emplace_back(readLine(file, numLine, line));
        }
    }
    file.close();
    if (numLine == 0)
    {
        std::cout << "There is no data in the file '" << this->fileName << "'\n";
    }
}

vector<string> ReadTxt::readLine(std::ifstream &f, int numLine, string line)
{
    char delims[] = " ,;|\t\n";
    char s[line.size() + 1];
    strcpy(s, line.c_str());
    char * st = strtok(s, delims);
    vector<string> dataLine;

    while(st != NULL)
    {
        dataLine.emplace_back(st);
        st = strtok(NULL, delims);
    }
    return dataLine;
}

vector<ProximityPair<string>>& ReadTxt::readList()
{
    vector<ProximityPair<string>>& a = *new vector<ProximityPair<string>>;
    return a;
}

vector<ProximityPair<string>>& ReadTxt::readMatrix()
{
    bool columnHeader;
    bool lowerTriangular;

    vector<vector<string>>::iterator* iter = new vector<vector<string>>::iterator(this->dataList.begin());
    int numLines = dataList.size();
    int numCols = std::max(dataList.at(0).size(), dataList.at(numLines - 1).size());
    int numLine = 1;
    if (numLines < numCols)
    {
        // Header in first column
        columnHeader = true;
        numElements = numLines;
        names.clear();
        names.resize(numElements);
        if (dataList.at(0).size() == 2)
            lowerTriangular = true;
        else
            lowerTriangular = false;
    }
    else
    {
        columnHeader = false;
        numElements = numCols;
        if (numLines > numCols)
        {
            // Header in the first line
            names = **iter;
            *iter = std::next(*iter, 1);
            numLine++;
            if (dataList.at(1).size() == 1)
            {
                // LOwer triangel matrix
                lowerTriangular = true;
            }
            else
                lowerTriangular = false;
        }
        else
        {
            // Without headers(numLines == numCols)
            names.clear();
            names.resize(numElements);
            for (int n = 1; n <= numElements; n++)
            {
                names[n - 1] = std::to_string(n);
            }
            if (dataList.at(0).size() == 1)
            {
                // Lower triangular matrix
                lowerTriangular = true;
            } else {
                lowerTriangular = false;
            }
        }
    }
    vector<ProximityPair<string>>* lst;
    lst = &readTable(columnHeader, lowerTriangular, numCols, numLine, *iter);
    delete iter;
    return *lst;
}

vector<ProximityPair<string>>& ReadTxt::readTable(bool columnHeaders, bool lowerTriangular, int numCols,
        int numLine, vector<vector<string>>::iterator& iter)
{
    double table[numElements][numElements];
    int row = 0;

    while(iter != dataList.end())
    {
        if (!columnHeaders &&(row >= numCols))
        {
            perror("The number of rows is larger than the number of columns");
            abort();
        }
        vector<string> tmp = *iter;
        for (int col = 0; col <(int) tmp.size(); col++)
        {
            if (columnHeaders &&(col == 0))
            {
                names.at(row) = tmp.at(0);
            }
            else if ((columnHeaders &&(col == row + 1)) ||(!columnHeaders &&(col == row)))
            {
                table[row][row] = atof(tmp.at(col).c_str());
            }
            else if (columnHeaders)
            {
                try
                {
                    table[row][col-1] = atof(tmp.at(col).c_str());
                    if (lowerTriangular)
                    {
                        table[col-1][row] = table[row][col-1];
                    }

                }
                catch(int e)
                {
                    perror("A non-numeric character has been found in a row or column larger than 1");
                    abort();
                }
            }
            else if (!columnHeaders)
            {
                try
                {
                    table[row][col] = atof(tmp.at(col).c_str());
                    if (lowerTriangular)
                    {
                        table[col][row] = table[row][col];
                    }
                }
                catch(int e)
                {
                    perror("A non-numeric character has been found in a row or column larger than 1");
                    abort();
                }
            }
        }
        iter = std::next(iter, 1);
        numLine++;
        row++;
    }
    // Check symmetry
    vector<ProximityPair<string>>& lst = *new vector<ProximityPair<string>>;
    for (int i = 0; i < numElements; i++)
    {
        ProximityPair<string> pp(names.at(i), names.at(i), table[i][i]);
        lst.emplace_back(pp);
        for (int j = i + 1; j < numElements; j++)
        {
            if (table[i][j] == table[j][i])
            {
                // pp se declara en el espacio de nombres actual para que se cree en la pila y no en el heap
                ProximityPair<string> pp(names.at(i), names.at(j), table[i][j]);
                lst.emplace_back(pp);
            }
            else
            {
                perror("Asymmetric data: two different values for the same data pair");
                abort();
            }
        }
    }
    return lst;
}

ReadTxt::~ReadTxt()
{
    delete this->data;
}
