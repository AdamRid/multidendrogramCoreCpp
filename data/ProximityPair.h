#ifndef PROXIMITYPAIR_H
#define PROXIMITYPAIR_H

#include <string>

template <class Element>
class ProximityPair
{
public:
    ProximityPair(const Element& element1, const Element& element2, const double proximity)
    {
        // Comprovar que realmene se guarda la referencia real al objeto
        this->element1 = element1;
        this->element2 = element2;
        this->proximity = proximity;
    }
    ProximityPair(const ProximityPair &p)
    {
        this->element1 = p.element1;
        this->element2 = p.element2;
        this->proximity = p.proximity;
    }

    ProximityPair& operator =(const ProximityPair& p)
    {
        return *this;
    }

    Element& getElement1()
    {
        return this->element1;
    }
    Element& getElement2()
    {
        return this->element2;
    }
    double getProximity()
    {
        return this->proximity;
    }
    std::string toString();

private:
    Element element1;
    Element element2;
    double proximity;

};

#endif
