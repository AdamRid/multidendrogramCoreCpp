#include "DataFile.h"
#include "../utils/prutils.h"


using std::string;

DataFile::DataFile()
{

}

DataFile::DataFile(const string& fileName)
{
	// string actualpath = realpath(fileName.c_str(), NULL);
	string actualpath = fileName;
    this->name = extractFileName(actualpath);
    this->path = extractPathName(actualpath);
    this->nameNoExt = getFileNameWithoutExtension(this->name);
}

DataFile::DataFile(const string& name, const string& path)
{
    this->name = name;
    this->path = path;
    this->nameNoExt = getFileNameWithoutExtension(name);
}

DataFile::DataFile(DataFile& fd)
{
    this->name = fd.getName();
    this->path = fd.getPath();
    this->nameNoExt = fd.getNameNoExt();
}

DataFile& DataFile::operator =(DataFile &fd)
{
    return *this;
}

string DataFile::getName()
{
    return this->name;
}

string DataFile::getNameNoExt()
{
    return this->nameNoExt;
}

void DataFile::setName(const string& name)
{
    this->name = name;
    this->nameNoExt = getFileNameWithoutExtension(name);
}

string DataFile::getPath()
{
    return this->path;
}

void DataFile::setPath(const string& path)
{
    this->path = path;
}

string DataFile::getPathName()
{
    return this->path + this->name;
}

string DataFile::getPathNameNoExt()
{
    return this->path + this->nameNoExt;
}

string DataFile::getFileNameWithoutExtension(const string& fileName)
{
    char sep = '.';

    size_t i = fileName.rfind(sep, fileName.length());
    if (i != string::npos)
    {
        return(fileName.substr(0, i));
    }

    return("");
}

string DataFile::extractPathName(const string& fileName) {

   char sep = '/';

    #ifdef _WIN32
    sep = '\\';
    #endif

   size_t i = fileName.rfind(sep, fileName.length());
   if (i != string::npos)
   {
        return(fileName.substr(0, i) + sep);
   }
   return("");
}


string DataFile::extractFileName(const string& fileName) {

    char sep = '/';

    #ifdef _WIN32
    sep = '\\';
    #endif

    size_t i = fileName.rfind(sep, fileName.length());
    if (i != string::npos)
    {
        return(fileName.substr(i + 1, fileName.length()));
    }
    return("");
}
