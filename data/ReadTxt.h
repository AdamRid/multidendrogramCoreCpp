#ifndef READTXT_H
#define READTXT_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "ProximityPair.h"

using std::string;
using std::vector;

class ReadTxt
{
public:
    ReadTxt(const string& filePath);
    int getNumElements();
    vector<ProximityPair<string>>& getData();
    ~ReadTxt();

public:
	string fileName;
	vector<vector<string>> dataList;
	int numElements = 0;
	vector<string> names;
	vector<ProximityPair<string>>* data;
	double missingValue = 0.0;

	void saveInMemory();
	vector<string> readLine(std::ifstream &f, int numLine, string line);
	vector<ProximityPair<string>>& readList();
	vector<ProximityPair<string>>& readMatrix();
	vector<ProximityPair<string>>& readTable(bool columnHeaders, bool lowerTriangular, int numCols,
			int numLine, vector<vector<string>>::iterator& iter);

};

#endif
