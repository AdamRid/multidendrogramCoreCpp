#ifndef EXTERNALDATA_H
#define EXTERNALDATA_H

#include "../gbincludes.h"

#include "../definitions/Dendrogram.h"
#include "../definitions/SymmetricMatrix.h"
#include "DataFile.h"
#include "ProximityPair.h"


class ExternalData
{
public:
    ExternalData(DataFile& datafile);
    DataFile& getDataFile();
    vector<string>& getNames();
    int getNumberOfElements();
    SymmetricMatrix& getProximityMatrix();
    vector<ProximityPair<string>>& getData();
    int getPrecision();
    ~ExternalData();

private:
    DataFile dataFile;
	vector<ProximityPair<string>> proximityPairs;
	int numElements = 0;
	SymmetricMatrix* proximityMatrix;
	map<string, int> hashNames;
	vector<string> names;
};

#endif
