#include "ExternalData.h"
#include "ReadTxt.h"

#include "../gbincludes.h"

ExternalData::ExternalData(DataFile& dataFile)
{
    this->dataFile.setName(dataFile.getName());
    this->dataFile.setPath(dataFile.getPath());
    ReadTxt txt(dataFile.getPathName());
    this->numElements = txt.getNumElements();
    this->proximityPairs = txt.getData();
    this->proximityMatrix = new SymmetricMatrix(this->numElements);
    int nextId = 0;
    vector<ProximityPair<string>>::iterator iterPairs = this->proximityPairs.begin();
    while(iterPairs != this->proximityPairs.end())
    {
        ProximityPair<string> pair_ = *iterPairs;
        int id1;
        string name1 = pair_.getElement1();
        map<string, int>::iterator iterName = this->hashNames.find(name1);
        if (iterName != this->hashNames.end())
        {
            id1 = iterName->second;
        }
        else
        {
            id1 = nextId;
            nextId++;
            this->hashNames[name1] = id1;
            this->names.emplace_back(name1);
        }

        int id2;
        string name2 = pair_.getElement2();
        iterName = this->hashNames.find(name2);
        if (iterName != this->hashNames.end())
        {
            id2 = iterName->second;
        }
        else
        {
            id2 = nextId;
            nextId++;
            this->hashNames[name2] = id2;
            this->names.emplace_back(name2);
        }
        double proximity = pair_.getProximity();
        this->proximityMatrix->setElement(id1, id2, proximity);
        iterPairs = std::next(iterPairs, 1);
    }
}

DataFile& ExternalData::getDataFile()
{
    return this->dataFile;
}

vector<string>& ExternalData::getNames()
{
    return this->names;
}

int ExternalData::getNumberOfElements()
{
    return this->numElements;
}

SymmetricMatrix& ExternalData::getProximityMatrix()
{
    return *this->proximityMatrix;
}

vector<ProximityPair<string>>& ExternalData::getData()
{
    return this->proximityPairs;
}

int ExternalData::getPrecision()
{
    return this->proximityMatrix->getPrecision();
}

ExternalData::~ExternalData()
{
    delete this->proximityMatrix;
}
