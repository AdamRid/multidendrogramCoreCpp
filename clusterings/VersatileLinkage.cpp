#include "VersatileLinkage.h"


VersatileLinkage::VersatileLinkage(SymmetricMatrix& proximityMatrix, vector<string>& labels,
    bool isDistanceBased, int precision, bool isWeighted,
    double power)
: HierarchicalClustering(proximityMatrix, labels, isDistanceBased,
    precision)
{
    this->isWeighted = isWeighted;
    this->power = power;
}

double VersatileLinkage::calculateProximity(Dendrogram& cI, Dendrogram& cJ)
{
    if (power == -infinity)
    {
        return minimumProximity(cI, cJ);
    }
    else if (power == infinity)
    {
        return maximumProximity(cI, cJ);
    }
    else if (power == 0.0)
    {
        return geometricMean(cI, cJ);
    }
    else
    {
        return generalizedMean(cI, cJ);
    }
}


double VersatileLinkage::geometricMean(Dendrogram& cI, Dendrogram& cJ)
{
    double proximity = 1.0;
    int numSubrootsI = cI.numberOfSubroots();
    int numSubrootsJ = cJ.numberOfSubroots();
    int numLeavesI = cI.numberOfLeaves();
    int numLeavesJ = cJ.numberOfLeaves();
    for (int i = 0; i < numSubrootsI; i ++)
    {
        Dendrogram subcI = cI.getSubroot(i);
        double wI = isWeighted ?  1.0 /(double) numSubrootsI :
           (double) subcI.numberOfLeaves() /(double) numLeavesI;
        for (int j = 0; j < numSubrootsJ; j ++)
        {
            Dendrogram subcJ = cJ.getSubroot(j);
            double wJ = isWeighted ? 1.0 /(double)
            numSubrootsJ :(double) subcJ.numberOfLeaves() /
           (double) numLeavesJ;
            double prox = rootsProximity(subcI, subcJ);
            proximity *= std::pow(prox, wI * wJ);
        }
    }
    return proximity;
}

double VersatileLinkage::generalizedMean(Dendrogram& cI, Dendrogram& cJ)
{
    int numSubrootsI = cI.numberOfSubroots();
    int numSubrootsJ = cJ.numberOfSubroots();
    int numLeavesI = cI.numberOfLeaves();
    int numLeavesJ = cJ.numberOfLeaves();
    double proximity = 0.0;
    for (int i = 0; i < numSubrootsI; i ++)
    {
        Dendrogram subcI = cI.getSubroot(i);
        double wI = isWeighted ? 1.0 /(double)numSubrootsI :
           (double) subcI.numberOfLeaves() /(double) numLeavesI;
        for (int j = 0; j < numSubrootsJ; j ++)
        {
            Dendrogram subcJ = cJ.getSubroot(j);
            double wJ = isWeighted ? 1.0 /(double) numSubrootsJ :
               (double) subcJ.numberOfLeaves() /(double)
                numLeavesJ;
            double prox = rootsProximity(subcI, subcJ);
            proximity += wI * wJ * std::pow(prox, power);
        }
    }
    proximity = std::pow(proximity, 1.0 / power);
    return proximity;
}

