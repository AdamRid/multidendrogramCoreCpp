#ifndef VERSATILELINKAGE_H_INCLUDED
#define VERSATILELINKAGE_H_INCLUDED

#include "HierarchicalClustering.h"

class VersatileLinkage : public HierarchicalClustering
{
public:
	VersatileLinkage (SymmetricMatrix& proximityMatrix, vector<string>& labels,
		bool isDistanceBased, int precision, bool isWeighted,
		double power);

protected:
	double calculateProximity (Dendrogram& cI, Dendrogram& cJ);

private:
	bool isWeighted;
	double power;

	double geometricMean (Dendrogram& cI, Dendrogram& cJ);
	double generalizedMean (Dendrogram& cI, Dendrogram& cJ);
};

#endif // VERSATILELINKAGE_H_INCLUDED
