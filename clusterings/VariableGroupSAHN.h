#ifndef VARIABLEGROUPSAHN_H_INCLUDED
#define VARIABLEGROUPSAHN_H_INCLUDED

#include "../gbincludes.h"
#include "../types/MethodType.h"
#include "../definitions/SymmetricMatrix.h"
#include "../utils/MathUtils.h"

class VariableGroupSAHN
{
public:
	VariableGroupSAHN(MethodType& method, bool isWeighted,
        SymmetricMatrix& initialDistances, int precision);
private:
    class Cluster
    {
    public:
        // Indicator of agglomerable cluster
        bool isAgglomerable;
        // Cluster cardinality
        int nMembers;
        // Distance to nearest neighbours "to the right"
        double distanceNN;
        // Nearest neighbours "to the right"
        vector<int> nearestNeighbours;
        // Clusters that the current one is nearest neighbour of
        vector<int> nearestNeighbourOf;

        Cluster();
    };

    class Agglomeration
    {
    public:
        double distance;
        vector<int> clusters;

        Agglomeration(double distance, int clusterA);
    };

    // Number of objects being clustered
	int nObjects;
	// Sequence of agglomerations
	vector<Agglomeration> agglomerations;

	vector<Cluster> initialPartition(SymmetricMatrix& distances,
        int precision);

    int clusterMinimumDistance(vector<Cluster>& clusters);

    Agglomeration newAgglomeration(MethodType& method, int precision,
        vector<Cluster>& clusters, int im);

    void connectedComponent(int precision, vector<Cluster>& clusters, int im,
        int jm, Agglomeration& agglomeration, vector<bool>& isConnected);

    void tieNearestNeighbours(int precision, vector<Cluster>& clusters,
        int im, Agglomeration& agglomeration, vector<bool> isConnected,
        vector<int>& nnList);

    void updateDistances(MethodType& method, bool isWeighted,
        SymmetricMatrix& distances, int precision, vector<Cluster>& clusters,
        int im, Agglomeration& agglomeration);

    double newDistance(MethodType& method, bool isWeighted,
        SymmetricMatrix& distances, vector<Cluster>& clusters,
        Agglomeration& agglomeration, int k);

    void updateNearestNeighbours(SymmetricMatrix& distances,
        int precision, vector<Cluster>& clusters, Agglomeration& agglomeration);

    void setNearestNeighbours(SymmetricMatrix& distances, int precision,
			vector<Cluster>& clusters, int i);

    void clearNearestNeighbours(vector<Cluster>& clusters, int i);

    void connectNearestNeighbours(vector<Cluster>& clusters, int i, int j);

    SymmetricMatrix ultrametricMatrix();

    void groupPair(SymmetricMatrix& dist, int clusterA, int clusterB,
        double distAB);

    vector<int> subclusters(SymmetricMatrix& dist, int i);
};


#endif
