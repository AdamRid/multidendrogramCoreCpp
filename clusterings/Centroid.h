#ifndef CENTROID_H_INCLUDED
#define CENTROID_H_INCLUDED

#include "LanceWilliams.h"

class Centroid : public LanceWilliams
{
public:
	Centroid (SymmetricMatrix& proximityMatrix, vector<string>& labels,
		bool isDistanceBased, int precision, bool isWeighted);

protected:
	double getAlpha(Dendrogram& cI, Dendrogram& subcI, Dendrogram& cJ,
		Dendrogram& subcJ);
	double getBeta(Dendrogram& cI, Dendrogram& subcI, Dendrogram& subcK,
		Dendrogram& cJ);

private:
	bool isWeighted;
};


#endif // CENTROID_H_INCLUDED
