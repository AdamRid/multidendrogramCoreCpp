#include "Ward.h"


Ward::Ward (SymmetricMatrix& proximityMatrix, vector<string>& labels,
    bool isDistanceBased, int precision)
: LanceWilliams (proximityMatrix, labels, isDistanceBased,
    precision)
{ }


double Ward::getAlpha(Dendrogram& cI, Dendrogram& subcI, Dendrogram& cJ,
    Dendrogram& subcJ)
{
    return
    (double) (subcI.numberOfLeaves () + subcJ.numberOfLeaves ())
    / (double) (cI.numberOfLeaves () + cJ.numberOfLeaves ());
}

double Ward::getBeta(Dendrogram& cI, Dendrogram& subcI, Dendrogram& subcK,
    Dendrogram& cJ)
{
    return
    -((double) cJ.numberOfLeaves () / (double) cI.numberOfLeaves ())
    * (double) (subcI.numberOfLeaves () + subcK.numberOfLeaves ())
    / (double) (cI.numberOfLeaves () + cJ.numberOfLeaves ());
}

