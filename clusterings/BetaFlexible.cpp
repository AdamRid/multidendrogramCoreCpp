#include "BetaFlexible.h"


BetaFlexible::BetaFlexible(SymmetricMatrix& proximityMatrix, vector<string>& labels,
    bool isDistanceBased, int precision, bool isWeighted,
    double beta)
: LanceWilliams(proximityMatrix, labels, isDistanceBased,
    precision)
{
    this->isWeighted = isWeighted;
    this->beta = beta;
}


double BetaFlexible::getAlpha(Dendrogram& cI, Dendrogram& subcI, Dendrogram& cJ,
    Dendrogram& subcJ)
{
    double wI = isWeighted ?
            1.0 /(double) cI.numberOfSubroots() :(double)
            subcI.numberOfLeaves() /(double) cI.numberOfLeaves();
    double wJ = isWeighted ?
            1.0 /(double)cJ.numberOfSubroots() :(double)
            subcJ.numberOfLeaves() /(double) cJ.numberOfLeaves();
    return wI * wJ *(1.0 - beta);
}

double BetaFlexible::getBeta(Dendrogram& cI, Dendrogram& subcI, Dendrogram& subcK,
    Dendrogram& cJ)
{
    double w;
    if (isWeighted)
    {
        int nI = cI.numberOfSubroots();
        int nJ = cJ.numberOfSubroots();
        w = 1.0 /(double)((nI - 1) * nI / 2 +(nJ - 1) * nJ / 2);
    }
    else
    {
        w =(double)(subcI.numberOfLeaves() *
            subcK.numberOfLeaves()) /(sigma(cI) + sigma(cJ));
    }
    return w * beta;
}

double BetaFlexible::sigma(Dendrogram& c)
{
    int s = c.numberOfLeaves() * c.numberOfLeaves();
    for (int i = 0; i < c.numberOfSubroots(); i ++)
    {
        Dendrogram subc = c.getSubroot(i);
        s -= subc.numberOfLeaves() * subc.numberOfLeaves();
    }
    return 0.5 *(double) s;
}

