#ifndef SINGLELINKAGE_H_INCLUDED
#define SINGLELINKAGE_H_INCLUDED

#include "HierarchicalClustering.h"

class SingleLinkage : public HierarchicalClustering
{
public:
	SingleLinkage (SymmetricMatrix& proximityMatrix, vector<string>& labels,
		bool isDistanceBased, int precision);
protected:
	double calculateProximity(Dendrogram& cI, Dendrogram& cJ);
};


#endif // SINGLELINKAGE_H_INCLUDED
