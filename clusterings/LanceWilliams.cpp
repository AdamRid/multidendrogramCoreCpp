#include "LanceWilliams.h"

using std::string;

LanceWilliams::LanceWilliams (SymmetricMatrix& proximityMatrix, vector<string>& labels,
    bool isDistanceBased, int precision)
: HierarchicalClustering (proximityMatrix, labels, isDistanceBased,
    precision)
{ }

double LanceWilliams::calculateProximity (Dendrogram& cI, Dendrogram& cJ)
{
    return alphaTerm (cI, cJ) + betaTerm (cI, cJ) +
        betaTerm (cJ, cI);
}

double LanceWilliams::alphaTerm (Dendrogram& cI, Dendrogram& cJ)
{
    double proximity = 0.0;
    for (int i = 0; i < cI.numberOfSubroots (); i ++)
    {
        Dendrogram subcI = cI.getSubroot (i);
        for (int j = 0; j < cJ.numberOfSubroots(); j ++)
        {
            Dendrogram subcJ = cJ.getSubroot (j);
            double alpha = getAlpha (cI, subcI, cJ, subcJ);
            double prox = rootsProximity (subcI, subcJ);
            proximity += alpha * prox;
        }
    }
    return proximity;
}

double LanceWilliams::betaTerm (Dendrogram& cI, Dendrogram& cJ)
{
    double proximity = 0.0;
    for (int i = 0; i < cI.numberOfSubroots() - 1; i ++)
    {
        Dendrogram subcI = cI.getSubroot (i);
        for (int k = i + 1; k < cI.numberOfSubroots (); k ++)
        {
            Dendrogram subcK = cI.getSubroot (k);
            double beta = getBeta (cI, subcI, subcK, cJ);
            double prox = rootsProximity (subcI, subcK);
            proximity += beta * prox;
        }
    }
    return proximity;
}

