#ifndef COMPLETELINKAGE_H_INCLUDED
#define COMPLETELINKAGE_H_INCLUDED

#include "HierarchicalClustering.h"

class CompleteLinkage : public HierarchicalClustering
{
public:
	CompleteLinkage (SymmetricMatrix& proximityMatrix, vector<string>& labels,
		bool isDistanceBased, int precision);

protected:
	double calculateProximity (Dendrogram& cI, Dendrogram& cJ);
};

#endif // COMPLETELINKAGE_H_INCLUDED
