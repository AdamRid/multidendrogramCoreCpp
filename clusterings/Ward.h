#ifndef WARD_H_INCLUDED
#define WARD_H_INCLUDED

#include "LanceWilliams.h"

class Ward : public LanceWilliams
{
public:
	Ward (SymmetricMatrix& proximityMatrix, vector<string>& labels,
		bool isDistanceBased, int precision);

protected:
	double getAlpha(Dendrogram& cI, Dendrogram& subcI, Dendrogram& cJ,
		Dendrogram& subcJ);
	double getBeta(Dendrogram& cI, Dendrogram& subcI, Dendrogram& subcK,
		Dendrogram& cJ);
};

#endif // WARD_H_INCLUDED
