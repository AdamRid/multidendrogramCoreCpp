#ifndef HIERARCHICALCLUSTERING_H
#define HIERARCHICALCLUSTERING_H
#include "../gbincludes.h"

#include "../definitions/Dendrogram.h"
#include "../definitions/SymmetricMatrix.h"

class HierarchicalClustering
{
public:
	HierarchicalClustering(SymmetricMatrix& proximityMatrix,
		vector<string>& labels, bool isDistanceBased, int precision);
	void build();
	void iteration();
	int numberOfRoots();
	Dendrogram& getRoot();
	~HierarchicalClustering();

protected:
	bool isDistanceBased;
	map<int, int> rootsToIndexes;

	double rootsProximity(Dendrogram& root1, Dendrogram& root2);
	virtual double calculateProximity(Dendrogram& cI, Dendrogram& cJ) = 0;
	double minimumProximity(Dendrogram& cI, Dendrogram& cJ);
	double maximumProximity(Dendrogram& cI, Dendrogram& cJ);

private:
	int precision;
	int nextClusterId;
	vector<Dendrogram> *roots;
	SymmetricMatrix rootsMatrix;

	const int NULL_GROUP = 0;

	vector<int> groupRoots();
	bool isInRange(double proximity);
	vector<Dendrogram>* createNewRoots(vector<int>& groups);
	double groupingProximity();
	void updateInternalProximities(vector<Dendrogram>& newRoots);
	SymmetricMatrix newProximityMatrix(vector<int>& groups,
		vector<Dendrogram>& newRoots);
	void mapRootsToIndexes();
};

#endif
