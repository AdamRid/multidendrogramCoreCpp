#include "../gbincludes.h"
#include "HierarchicalClustering.h"
#include "../utils/MathUtils.h"




HierarchicalClustering::HierarchicalClustering(SymmetricMatrix &proximityMatrix,
    vector<string>& labels, bool isDistanceBased, int precision)
{
    nextClusterId = 1;
    this->isDistanceBased = isDistanceBased;
    this->precision = precision;
    // Initialize roots
    int numElements = proximityMatrix.numberOfRows();
    this->roots = new vector<Dendrogram>;
    this->roots->reserve(numElements);
    for (int n = 0; n < numElements; n ++)
    {
        Dendrogram root(this->nextClusterId, labels[n], isDistanceBased, precision);
        this->nextClusterId ++;
        double proximity = proximityMatrix.getElement(n, n);
        if (!std::isnan(proximity)) {
            root.setRootHeights(proximity);
            root.setNodesHeights(proximity);
        }
        this->roots->push_back(root);
    }
    mapRootsToIndexes();
    // Inicializacion con el 'constructor por copia
    this->rootsMatrix = proximityMatrix;
}

void HierarchicalClustering::build()
{
    while(numberOfRoots() > 1)
    {
        iteration();
    }
}

void HierarchicalClustering::iteration()
{
    vector<int> groups = groupRoots();
    vector<Dendrogram>* newRoots = createNewRoots(groups);
    updateInternalProximities(*newRoots);
    rootsMatrix = newProximityMatrix(groups, *newRoots);
    delete this->roots;
    roots = newRoots;
    mapRootsToIndexes();
}

int HierarchicalClustering::numberOfRoots()
{
    return roots->size();
}

Dendrogram& HierarchicalClustering::getRoot()
{
    return(*roots)[0];
}


double HierarchicalClustering::rootsProximity(Dendrogram& root1,
    Dendrogram& root2)
{
    int index1 = rootsToIndexes[root1.getIdentifier()];
    int index2 = rootsToIndexes[root2.getIdentifier()];
    return rootsMatrix.getElement(index1, index2);
}

double HierarchicalClustering::minimumProximity(Dendrogram& cI,
    Dendrogram& cJ)
{
    double minimum  = infinity;
    for (int i = 0; i < cI.numberOfSubroots(); i++)
    {
        Dendrogram subcI = cI.getSubroot(i);
        for (int j = 0; j < cJ.numberOfSubroots(); j++)
        {
            Dendrogram subcJ = cJ.getSubroot(j);
            double proximity = rootsProximity(subcI, subcJ);
            minimum = std::min(minimum, proximity);
        }
    }
    return minimum;
}

double HierarchicalClustering::maximumProximity(Dendrogram& cI,
    Dendrogram& cJ)
{
    double maximum = -infinity;
    for (int i = 0; i < cI.numberOfSubroots(); i++)
    {
        Dendrogram subcI = cI.getSubroot(i);
        for (int j = 0; j < cJ.numberOfSubroots(); j++)
        {
            Dendrogram subcJ = cJ.getSubroot(j);
            double proximity = rootsProximity(subcI, subcJ);
            maximum = std::max(maximum, proximity);
        }
    }
    return maximum;
}



vector<int> HierarchicalClustering::groupRoots()
{
    // Initialize groups
    int numRoots = roots->size();
    vector<int> groups(numRoots, NULL_GROUP);
    // Put each root in a group.
    // If they have to be merged, they are put in the same group.
    int nextGroupId = 1;
    for (int i = 0; i < numRoots - 1; i++)
    {
        for (int j = i + 1; j < numRoots; j++)
        {
            double proximity = rootsMatrix.getElement(i, j);
            if (isInRange(proximity))
            {
                // Merge groups at minimum distance(or maximum
                // similarity)
                if ((groups[i] == NULL_GROUP) &&
                  (groups[j] == NULL_GROUP))
                {
                    // If both roots do not have any group
                    // identifier, then new group identifier
                    groups[i] = nextGroupId;
                    groups[j] = nextGroupId;
                    nextGroupId++;
                }
                else if ((groups[i] == NULL_GROUP) &&
                         (groups[j] != NULL_GROUP))
                {
                    groups[i] = groups[j];
                }
                else if ((groups[i] != NULL_GROUP) &&
                         (groups[j] == NULL_GROUP))
                {
                    groups[j] = groups[i];
                }
                else
                {
                    // Both roots have group identifiers
                    if (groups[i] != groups[j])
                    {
                        // Merge the two roots in the same group
                        for (int k = 0; k < numRoots; k++)
                        {
                            if (groups[k] == groups[j])
                            {
                                groups[k] = groups[i];
                            }
                        }
                    }
                }
            }
        }
    }
    return groups;
}

bool HierarchicalClustering::isInRange(double proximity)
{
    const double epsilon = 1.0 / std::pow(10, precision + 1);
    double value = MathUtils::round(proximity, precision);
    double bound = MathUtils::round(groupingProximity(),
        precision);
    return(std::abs(value - bound) < epsilon);
}

vector<Dendrogram>* HierarchicalClustering::createNewRoots(vector<int>& groups)
{
    map<int, Dendrogram> groupsToRoots;
    vector<int> keysMap;
    for (int n = 0; n <(int) groups.size(); n++)
    {
        Dendrogram& cluster =(*roots)[n];
        if (groups[n] == NULL_GROUP)
        {
            cluster.setSupercluster(false);
            groupsToRoots[-cluster.getIdentifier()] = cluster;
            keysMap.emplace_back(-cluster.getIdentifier());
        }
        else
        {
            // Add cluster to the corresponding supercluster
            if (groupsToRoots.count(groups[n]))
            {
                Dendrogram& supercluster = groupsToRoots[groups[n]];
                supercluster.addSubcluster(cluster);
            }
            else
            {
                string label = std::to_string(nextClusterId);
                Dendrogram d(nextClusterId, label, isDistanceBased, precision);
                groupsToRoots[groups[n]] = d;
                nextClusterId++;
                groupsToRoots[groups[n]].setRootHeights(groupingProximity());
                groupsToRoots[groups[n]].setBandsHeights(groupingProximity());
                groupsToRoots[groups[n]].addSubcluster(cluster);
                keysMap.emplace_back(groups[n]);
            }
        }
    }
    vector<Dendrogram>* newRoots =  new vector<Dendrogram>;
    for (int n = 0; n <(int) keysMap.size(); n++)
    {
        newRoots->emplace_back(groupsToRoots[keysMap[n]]);
    }
    return newRoots;
}

double HierarchicalClustering::groupingProximity()
{
    return isDistanceBased ? rootsMatrix.minimumValue() :
        rootsMatrix.maximumValue();
}

void HierarchicalClustering::updateInternalProximities(vector<Dendrogram>& newRoots)
{
    for (int n = 0; n <(int) newRoots.size(); n ++)
    {
        Dendrogram& root = newRoots[n];
        int numSubroots = root.numberOfSubroots();
        if (numSubroots > 2)
        {
            double rootTopHeight = isDistanceBased ?
                    -infinity : infinity;
            for (int i = 0; i < numSubroots - 1; i ++)
            {
                Dendrogram& subrI = root.getSubroot(i);
                for (int j = i + 1; j < numSubroots; j ++)
                {
                    Dendrogram& subrJ = root.getSubroot(j);
                    double proximity = rootsProximity(subrI, subrJ);
                    rootTopHeight = isDistanceBased ?
                        std::max(rootTopHeight, proximity) :
                        std::min(rootTopHeight, proximity);
                }
            }
            root.setRootTopHeight(rootTopHeight);
        }
    }
}

SymmetricMatrix HierarchicalClustering::newProximityMatrix(vector<int>& groups,
    vector<Dendrogram>& newRoots)
{
    int numRoots =(int) newRoots.size();
    SymmetricMatrix newMatrix(numRoots);
    for (int i = 0; i < numRoots; i ++)
    {
        Dendrogram& rootI = newRoots[i];
        double proximity = rootI.getRootBottomHeight();
        newMatrix.setElement(i, i, proximity);
        for (int j = i + 1; j < numRoots; j ++)
        {
            Dendrogram& rootJ = newRoots[j];
            // Calculations only if there is a new cluster
            if (rootI.isSupercluster() || rootJ.isSupercluster())
            {
                proximity = calculateProximity(rootI, rootJ);
            }
            else
            {
                proximity = rootsProximity(rootI, rootJ);
            }
            newMatrix.setElement(i, j, proximity);
        }
    }
    return newMatrix;
}

void HierarchicalClustering::mapRootsToIndexes()
{
    rootsToIndexes.clear();
    for (int n = 0; n <(int) roots->size(); n++)
    {
        Dendrogram& root =(*roots)[n];
        rootsToIndexes[root.getIdentifier()] = n;
    }
}

HierarchicalClustering::~HierarchicalClustering()
{
    delete this->roots;
}

