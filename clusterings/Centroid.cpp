#include "Centroid.h"

Centroid::Centroid (SymmetricMatrix& proximityMatrix, vector<string>& labels,
    bool isDistanceBased, int precision, bool isWeighted)
: LanceWilliams (proximityMatrix, labels, isDistanceBased,
    precision)
{
    this->isWeighted = isWeighted;
}

double Centroid::getAlpha(Dendrogram& cI, Dendrogram& subcI, Dendrogram& cJ,
    Dendrogram& subcJ)
{
    double alpha = isWeighted ? +1.0 / (double)
    (cI.numberOfSubroots () * cJ.numberOfSubroots ()) :
    + (double) (subcI.numberOfLeaves () * subcJ.numberOfLeaves ()) /
    (double) (cI.numberOfLeaves () * cJ.numberOfLeaves ());

    return alpha;
}

double Centroid::getBeta(Dendrogram& cI, Dendrogram& subcI, Dendrogram& subcK,
    Dendrogram& cJ)
{
    double beta = isWeighted ? -1.0 / (double)
    (cI.numberOfSubroots () * cI.numberOfSubroots ()) : -(double)
    (subcI.numberOfLeaves () * subcK.numberOfLeaves ()) /
    (double) (cI.numberOfLeaves () * cI.numberOfLeaves ());

    return beta;
}

