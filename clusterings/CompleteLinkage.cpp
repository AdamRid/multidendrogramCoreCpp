#include "CompleteLinkage.h"

CompleteLinkage::CompleteLinkage (SymmetricMatrix& proximityMatrix, vector<string>& labels,
    bool isDistanceBased, int precision)
: HierarchicalClustering (proximityMatrix, labels, isDistanceBased,
    precision)
{ }


double CompleteLinkage::calculateProximity (Dendrogram& cI, Dendrogram& cJ)
{
    if (isDistanceBased)
    {
        return maximumProximity(cI, cJ);
    }
    else
    {
        return minimumProximity(cI, cJ);
    }
}

