#include "VariableGroupSAHN.h"

VariableGroupSAHN::Cluster::Cluster()
{
    this->isAgglomerable = true;
    this->nMembers = 1;
    this->distanceNN = infinity;
}

VariableGroupSAHN::Agglomeration::Agglomeration(double distance, int clusterA)
{
    this->distance = distance;
    this->clusters.emplace_back(clusterA);
}

VariableGroupSAHN::VariableGroupSAHN(MethodType& method, bool isWeighted,
    SymmetricMatrix& initialDistances, int precision)
{
    SymmetricMatrix distances(initialDistances);
    if (method == MethodType::WARD) {
        // Use squared distances
        // distances.square();
    }
    this->nObjects = distances.numberOfRows();
    vector<Cluster> clusters = initialPartition(distances, precision);
    int nClustered = 0;
    while (nClustered < this->nObjects - 1) {
        int im = clusterMinimumDistance(clusters);
        Agglomeration agglomeration = newAgglomeration(method, precision,
            clusters, im);
        this->agglomerations.emplace_back(agglomeration);
        nClustered = nClustered + (int) agglomeration.clusters.size() - 1;
        updateDistances(method, isWeighted, distances, precision, clusters,
                im, agglomeration);
        updateNearestNeighbours(distances, precision, clusters,
                agglomeration);
    }
}

vector<VariableGroupSAHN::Cluster> VariableGroupSAHN::initialPartition(SymmetricMatrix& distances,
    int precision)
{
    vector<Cluster> clusters(this->nObjects);
    for (int i = 0; i < (int)this->nObjects - 1; i ++)
    {
        this->setNearestNeighbours(distances, precision, clusters, i);
    }
    return clusters;
}

int VariableGroupSAHN::clusterMinimumDistance(vector<Cluster>& clusters)
{
    double dmin = infinity;
    int im = -1;
    for (int i = 0; i < (int)clusters.size() - 1; i ++)
    {
        if (clusters[i].isAgglomerable && (clusters[i].distanceNN < dmin))
        {
            dmin = clusters[i].distanceNN;
            im = i;
        }
    }
    return im;
}

VariableGroupSAHN::Agglomeration VariableGroupSAHN::newAgglomeration(MethodType& method, int precision,
    vector<Cluster>& clusters, int im)
{
    double dmin = clusters[im].distanceNN;
    if (method == MethodType::WARD)
    {
        // Undo squared distance when method is Ward
        dmin = sqrt(dmin);
    }
    Agglomeration agglomeration(dmin, im);
    vector<bool> isConnected ((int)clusters.size(), false);
    isConnected[im] = true;
    vector<int>::iterator nnIter =
            clusters[im].nearestNeighbours.begin();
    while (nnIter != clusters[im].nearestNeighbours.end())
    {
        int jm = *nnIter;
        this->connectedComponent(precision, clusters, im, jm, agglomeration,
                isConnected);
        std::advance(nnIter, 1);
    }
    std::sort(agglomeration.clusters.begin(), agglomeration.clusters.end());
    return agglomeration;
}

void VariableGroupSAHN::connectedComponent(int precision, vector<Cluster>& clusters, int im,
    int jm, Agglomeration& agglomeration, vector<bool>& isConnected)
{
    agglomeration.clusters.emplace_back(jm);
    isConnected[jm] = true;
    clusters[jm].isAgglomerable = false;
    this->tieNearestNeighbours(precision, clusters, im, agglomeration,
            isConnected, clusters[jm].nearestNeighbours);
    this->tieNearestNeighbours(precision, clusters, im, agglomeration,
            isConnected, clusters[jm].nearestNeighbourOf);
}

void VariableGroupSAHN::tieNearestNeighbours(int precision, vector<Cluster>& clusters,
    int im, Agglomeration& agglomeration, vector<bool> isConnected, vector<int>& nnList)
{
    const double dmin = clusters[im].distanceNN;
    vector<int>::iterator nnIter = nnList.begin();
    while (nnIter != nnList.end())
    {
        int jm = *nnIter;
        if ((!isConnected[jm]) &&
            MathUtils::areEqual(precision, clusters[jm].distanceNN, dmin))
        {
            this->connectedComponent(precision, clusters, im, jm, agglomeration,
                    isConnected);
        }
        std::advance(nnIter, 1);
    }
}

void VariableGroupSAHN::updateDistances(MethodType& method, bool isWeighted,
    SymmetricMatrix& distances, int precision, vector<Cluster>& clusters,
    int im, Agglomeration& agglomeration)
{
    double dmin = infinity;
    for (int k = 0; k < (int)clusters.size(); k++)
    {
        if (clusters[k].isAgglomerable && (k != im))
        {
            double dik = this->newDistance(method, isWeighted, distances,
                    clusters, agglomeration, k);
            distances.setElement(im, k, dik);
            if (im < k)
            {
                if (dik < dmin)
                {
                    dmin = dik;
                }
            } else
            {
                // Fix to ensure that correct nearest neighbours are found
                // using non-monotone clustering methods (e.g. centroid)
                double dkNN = clusters[k].distanceNN;
                if (MathUtils::areEqual(precision, dkNN, dik))
                {
                    this->connectNearestNeighbours(clusters, k, im);
                }
                else if (dik < dkNN)
                {
                    clusters[k].distanceNN = dik;
                    this->clearNearestNeighbours(clusters, k);
                    this->connectNearestNeighbours(clusters, k, im);
                }
            }
        }
    }
    vector<int>::iterator iter = agglomeration.clusters.begin();
    // Skip first nearest neighbour (current cluster)
    std::advance(iter, 1);
    while (iter != agglomeration.clusters.end())
    {
        int i = *iter;
        clusters[im].nMembers = clusters[im].nMembers +
                clusters[i].nMembers;
        clearNearestNeighbours(clusters, i);
        std::advance(iter, 1);
    }
    clusters[im].distanceNN = dmin;
    this->setNearestNeighbours(distances, precision, clusters, im);
}

double VariableGroupSAHN::newDistance(MethodType& method, bool isWeighted,
    SymmetricMatrix& distances, vector<Cluster>& clusters,
    Agglomeration& agglomeration, int k)
{
    vector<int>::iterator iter;
    double dik = 0.0;
    switch (method)
    {
    case SINGLE_LINKAGE:
        dik = infinity;
        iter = agglomeration.clusters.begin();
        while (iter != agglomeration.clusters.end())
        {
            int i = *iter;
            dik = std::min(dik, distances.getElement(i, k));
            std::advance(iter, 1);
        }
        break;
    case COMPLETE_LINKAGE:
        dik = -infinity;
        iter = agglomeration.clusters.begin();
        while (iter != agglomeration.clusters.end())
        {
            int i = *iter;
            dik = std::max(dik, distances.getElement(i, k));
            std::advance(iter, 1);
        }
        break;
    case ARITHMETIC_LINKAGE:
        break;
    case WARD:
        break;
    case CENTROID:
        break;
    default:
        break;
    }
    return dik;
}

void VariableGroupSAHN::updateNearestNeighbours(SymmetricMatrix& distances,
    int precision, vector<Cluster>& clusters, Agglomeration& agglomeration)
{
    for (int k = 0; k < (int) clusters.size() - 1; k++)
    {
        if (clusters[k].isAgglomerable)
        {
            bool hasNN = false;
            vector<int>::iterator nnIter =
                    clusters[k].nearestNeighbours.begin();
            while ((!hasNN) && nnIter != clusters[k].nearestNeighbours.end())
            {
                int kNN = *nnIter;
                bool contained = std::find(agglomeration.clusters.begin(),
                    agglomeration.clusters.end(), kNN) != agglomeration.clusters.end();
                hasNN = hasNN || contained;
                std::advance(nnIter, 1);
            }
            if (hasNN)
            {
                this->setNearestNeighbours(distances, precision, clusters, k);
            }
        }
    }
}

void VariableGroupSAHN::setNearestNeighbours(SymmetricMatrix& distances, int precision,
        vector<Cluster>& clusters, int i)
{
    double dNN = infinity;
    int jNN = -1;
    for (int j = i + 1; j < (int) clusters.size(); j ++)
    {
        if (clusters[j].isAgglomerable)
        {
            double dij = distances.getElement(i, j);
            if (dij < dNN)
            {
                dNN = dij;
                jNN = j;
            }
        }
    }
    clusters[i].distanceNN = dNN;
    this->clearNearestNeighbours(clusters, i);
    if (jNN > -1)
    {
        for (int j = jNN; j < (int) clusters.size(); j ++)
        {
            if (clusters[j].isAgglomerable)
            {
                double dij = distances.getElement(i, j);
                if (MathUtils::areEqual(precision, dij, dNN))
                {
                    this->connectNearestNeighbours(clusters, i, j);
                }
            }
        }
    }
}

void VariableGroupSAHN::clearNearestNeighbours(vector<Cluster>& clusters, int i)
{
    vector<int>::iterator nnIter =
            clusters[i].nearestNeighbours.begin();
    while (nnIter != clusters[i].nearestNeighbours.end())
    {
        int j = *nnIter;
        clusters[j].nearestNeighbourOf.erase(clusters[j].nearestNeighbourOf.begin() + i);
        std::advance(nnIter, 1);
    }
    clusters[i].nearestNeighbours.clear();  // En java se crea un nuevo objeto. Creo que no es necesario
}

void VariableGroupSAHN::connectNearestNeighbours(vector<Cluster>& clusters, int i, int j)
{
    clusters[i].nearestNeighbours.emplace_back(j);
    clusters[j].nearestNeighbourOf.emplace_back(i);
}

SymmetricMatrix VariableGroupSAHN::ultrametricMatrix()
{
    SymmetricMatrix dist(this->nObjects);
    for (int i = 0; i < this->nObjects; i++)
    {
        dist.setElement(i, i, 0.0);
    }
    vector<Agglomeration>::iterator agglomIter =
        this->agglomerations.begin();
    while (agglomIter != this->agglomerations.end())
    {
        Agglomeration& agglom = *agglomIter;
        vector<int>::iterator clusIter = agglom.clusters.begin();
        if (clusIter != agglom.clusters.end())
        {
            int clusterA = *clusIter;     // ¿Es seguro de que existe algún elemento en la lista?
            std::advance(clusIter, 1);
            while (clusIter != agglom.clusters.end())
            {
                int clusterB = *clusIter;
                this->groupPair(dist, clusterA, clusterB, agglom.distance);
            }
        }
        std::advance(agglomIter, 1);
    }
    return dist;
}

void VariableGroupSAHN::groupPair(SymmetricMatrix& dist, int clusterA, int clusterB,
    double distAB)
{
    vector<int> subcA = subclusters(dist, clusterA);
    vector<int> subcB = subclusters(dist, clusterB);
    vector<int>::iterator iterA = subcA.begin();
    while (iterA != subcA.end()) {
        int i = *iterA;
        vector<int>::iterator iterB = subcB.begin();
        while (iterB != subcB.end())
        {
            int j = *iterB;
            dist.setElement(i, j, distAB);
            std::advance(iterB, 1);
        }
        std::advance(iterA, 1);
    }
}

vector<int> VariableGroupSAHN::subclusters(SymmetricMatrix& dist, int i)
{
    vector<int> subc;
    for (int j = i; j < this->nObjects; j ++)
    {
        double dij = dist.getElement(i, j);
        if (!std::isnan(dij))
        {
            subc.emplace_back(j);
        }
    }
    return subc;
}
