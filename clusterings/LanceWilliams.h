#ifndef LANCEWILLIAMS_H
#define LANCEWILLIAMS_H

#include "HierarchicalClustering.h"


class LanceWilliams : public HierarchicalClustering
{
public:
	LanceWilliams (SymmetricMatrix& proximityMatrix, vector<string>& labels,
		bool isDistanceBased, int precision);

protected:
	double calculateProximity (Dendrogram& cI, Dendrogram& cJ);
	double alphaTerm (Dendrogram& cI, Dendrogram& cJ);
	virtual double getAlpha (Dendrogram& cI, Dendrogram& subcI,
		Dendrogram& cJ, Dendrogram& subcJ) = 0;
	virtual double getBeta (Dendrogram& cI, Dendrogram& subcI,
		Dendrogram& subcK, Dendrogram& cJ) = 0;

private:
	double betaTerm (Dendrogram& cI, Dendrogram& cJ);
};


#endif
