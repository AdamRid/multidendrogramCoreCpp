#include "SingleLinkage.h"

SingleLinkage::SingleLinkage (SymmetricMatrix& proximityMatrix, vector<string>& labels,
    bool isDistanceBased, int precision)
: HierarchicalClustering (proximityMatrix, labels, isDistanceBased,
    precision)
{ }


double SingleLinkage::calculateProximity(Dendrogram& cI, Dendrogram& cJ)
{
    if (isDistanceBased)
    {
        return minimumProximity(cI, cJ);
    }
    else
    {
        return maximumProximity(cI, cJ);
    }
}

