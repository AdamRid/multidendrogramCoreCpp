#ifndef BETAFLEXIBLE_H_INCLUDED
#define BETAFLEXIBLE_H_INCLUDED

#include "LanceWilliams.h"

class BetaFlexible : public LanceWilliams
{
public:
	BetaFlexible(SymmetricMatrix& proximityMatrix, vector<string>& labels,
		bool isDistanceBased, int precision, bool isWeighted,
		double beta);

protected:
	double getAlpha(Dendrogram& cI, Dendrogram& subcI, Dendrogram& cJ,
		Dendrogram& subcJ);
	double getBeta(Dendrogram& cI, Dendrogram& subcI, Dendrogram& subcK,
		Dendrogram& cJ);

private:
	bool isWeighted;
	double beta;

	double sigma(Dendrogram& c);
};


#endif // BETAFLEXIBLE_H_INCLUDED
